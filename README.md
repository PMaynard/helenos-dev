# HelenOS Developer VM

This is automates the build of [HelenOS](http://www.helenos.org/) within a virtual machine. The base OS is Ubuntu Linux. Use the provision command to rebuild any changes.

	vagrant up
	vagrant provision 
